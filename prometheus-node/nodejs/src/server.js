'use strict'
const express = require('express')
const Prometheus = require('prom-client')
const app = express()
const port = process.env.PORT || 8080

const invocation_count = new Prometheus.Counter({
  name: 'invocation_count',
  help: 'Total route invocations /info'
})

const uptime = new Prometheus.Counter({
  name: 'uptime',
  help: 'Uptime system'
})

setInterval(() => {
  uptime.inc()
}, 1000);

app.get('/info', (req, res, next) => {
  invocation_count.inc()
  res.json({ message: 'Hello World!' })
})

app.get('/metrics', (req, res) => {
  res.set('Content-Type', Prometheus.register.contentType)
  res.end(Prometheus.register.metrics())
})

const server = app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`)
})
