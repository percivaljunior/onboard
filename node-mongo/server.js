const express = require('express')
const mongoose = require('mongoose')
const app = express()

const {
  MONGO_USERNAME,
  MONGO_PASSWORD
} = process.env;

(function connectWithRetry() {
  const url = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@mongodb:27017`
  mongoose.connect(url)
    .then(() => console.log('connected'))
    .catch(err => {
      console.log('reconnect...')
      setTimeout(connectWithRetry, 5000)
    })
})()

const message = mongoose.Schema({
	message : { type: String }
})

const Model = mongoose.model('message', message)

app.get("/info", function (req, res) {
  Model.find().then(data => res.send(data))
})

app.listen(8080, function() {
  Model.create({ message: 'world' })
})
